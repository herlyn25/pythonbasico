#programa lee un entero positivo,n,introducido por el usuario y después muestre en pantalla la suma de todos los enteros desde 1 hasta n

minumero = input("Digite un numero entero positivo: ")
if minumero.isnumeric() and int(minumero) >= 1:
    aux = int(minumero)
    suma = (aux * (aux + 1)) / 2
    print("La sumatoria de 1 a", aux, "es", suma)

else:
    print("Digit only numbers enteros")
