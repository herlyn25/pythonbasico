def validar_float(numero):
    """Es capaz de comprobar si el parametro tiener formato decimal parteentera.parte decimal (0.0)
    @param numero: es el valor a validar que cumpla con el formato de un decimal
    @return: respuesta bool : que indica True si el valor no cumple con formato y False cuando lo cumple"""

    answer = True
    try:
        if numero.isalpha():
            print("Solo debe contener numeros separados por un punto. Ejemplo 58.59")
        elif numero.find(",") != -1:
            print("El numero solo debe llevar punto no comas. Ejemplo 58.59")
        elif numero.count(".") > 1 or numero.count(",") >= 1 or numero.count(" ") >= 1:
            print("Solo debe contener un punto. Ejemplo 58.59")
        elif numero.count(" ") >= 1:
            print("No debe contener espacios. Ejemplo 58.59")
        elif numero.isnumeric():
            answer = False
        elif float(numero):
            answer = False
        elif numero.isalnum():
            print("No debe contener letras y numeros. Ejemplo 58.59")
    except ValueError:
        print("Digite el formato correcto. Ejemplo 58.59")

    return answer


def imprimir_opciones(tipo: str):
    """
    muestra las opciondes de las monedas a convertir
    @param tipo: me indica si la moneda que pedira sera la inicial o la final
    @type str  tipo
    """
    print(f'*****Digite modeda {tipo}****')
    print("""
        1. Pesos Colombianos
        2. Dolares
        3. Euros
        4. Libras
        5. Peso Mexicano'    
        """)

def pedir_opciones_y_valor():
    """
    Es la función encargada de tomar las opciones de la modena inicial, la moneda a la que se desea convertir y
    el valor inicial de la moneda que se va a convertir.
    @return: tupla_valores: contiene modena inicial, la moneda a convertir y el valor de la moneda inicial
    """
    tupla_valores = ()
    arreglo_opciones = ["1", "2", "3", "4", "5"]

    imprimir_opciones("inicial")
    moneda_inicial = input("Digite la opcion moneda inicial: ")

    while moneda_inicial not in arreglo_opciones:
        print("Opcion no valida")
        imprimir_opciones("inicial")
        moneda_inicial = input("Digite la moneda inicial: ")

    imprimir_opciones("a convertir")
    moneda_a_convertir = input("Digite la opcion moneda a convertir: ")

    while moneda_a_convertir not in arreglo_opciones:
        print("Opcion no valida")
        imprimir_opciones("a convertir")
        moneda_a_convertir = input("Digite la opcion de moneda a convertir : ")

    cantidad_convertir = input("Digite la cantidad  a convertir: ")
    while validar_float(cantidad_convertir):
        cantidad_convertir = input("Digite la cantidad a convertir: ")

    tupla_valores = float(cantidad_convertir), moneda_inicial, moneda_a_convertir

    return tupla_valores


# def convertir_monedas(valor_moneda_a_convertir,moneda_inicial,moneda_a_convertir):
def convertir_monedas(*args):
    """
    Esta es la función que se encarga de hacer la conversión de las monedas
    @param args: es una tupla que contiene los parametros a recibir como lo es valor de moneda_inicial,
    moneda_a_convertir y el valor de la moneda inicial
    @return tupla_information:
    """

    moneda_convertida = 0
    arreglo_monedas = ["pesos", "dolar", "euro", "libras", "peso_mex"]
    diccionario_pesos = {"dolar": 3911, "euro": 4400, "libras": 5200, "peso_mex": 192}
    diccionario_monedas = {"1": "COP", "2": "USD", "3": "EUR", "4": "LIBRA", "5": "MXN"}

    if args[1] == "1" and args[2] != "1":
        moneda_convertida = args[0] / diccionario_pesos[arreglo_monedas[int(args[2]) - 1]]
    elif args[1] != "1" and args[2] == "1":
        moneda_convertida = args[0] * diccionario_pesos[arreglo_monedas[int(args[2]) - 1]]
    elif args[1] != "1" and args[2] != "1":
        moneda_convertida = args[0] * (diccionario_pesos[arreglo_monedas[int(args[1]) - 1]] / diccionario_pesos[
            arreglo_monedas[int(args[2]) - 1]])
    else:
        moneda_convertida = args[0]
    tupla_informacion = args[0], round(moneda_convertida, 2), diccionario_monedas[args[1]], diccionario_monedas[args[2]]

    return tupla_informacion


if __name__ == "__main__":
    tupla_valores = pedir_opciones_y_valor()
    tipo_moneda = convertir_monedas(*tupla_valores)
    print(f' La conversion fue la siguiente {tipo_moneda[0]} {tipo_moneda[2]} => {tipo_moneda[1]} {tipo_moneda[3]}')
