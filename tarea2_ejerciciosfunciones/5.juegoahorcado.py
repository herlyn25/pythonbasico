from random import choice
arreglo_palabras=["murcielago","avestruz","maquiavelo","salem","dinamarca","colombia","australia", "enciclopedia","paracaidas"]
def imprimir_ganaste():
    """
    Imprime solo la palabra ganaste

    """
    print("""
*******  ********  ***   **  ********  *******  ********  ********
*******  ********  ****  **  ********  *******  ********  ********
**       **    **  ** ** **  **    **  **          **     **
** ****  **    **  **  ****  **    **  ******      **     ******
** ****  ********  **   ***  ********  ******      **     ******
**   **  ********  **   ***  ********      **      **     **
*******  **    **  **   ***  **    **  ******      **     ********
*******  **    **  **   ***  **    **  ******      **     ********""")

def imprimir_perdiste():
    """
      Imprime solo la palabra perdiste

    """
    print("""
*******  ********  ********  *******   *******  *******  ********  ********
*******  ********  ********  ********  *******  *******  ********  ********
**   **  **        **    **  **    **    **     **          **     **
*******  *****     *******   **    **    **     ******      **     ******
*******  *****     ******    **    **    **     ******      **     ******
**       **        **   **   **    **    **         **      **     **
**       *******   **    **  *******   ******   ******      **     ********
**       *******   **    **  ******    ******   ******      **     ********
    """)

def imprimir_muneco(intentos,palabra):
    """
    :Esta funcion se encarga de mostrar el muñeco cada vez que el jugador falle un intento al colocar una letra que no
    correspone a  la palabra a adivinar
    @param intentos: es el numero de vidas que tiene el participante, con este valida para ir dibujando el muñeco
    @param palabra: se utiliza para cuando acabe los intentos muestre muñeco completo y la palabra que tenia que adivinar
    """
    if intentos==1:
        print("                 O")
    elif intentos==2:
        print("                 O")
        print("                 |")
    elif intentos==3:
        print("                 O")
        print("                /|")
    elif intentos==4:
        print("                 O")
        print("                /|\ ")
    elif intentos==5:
        print("                 O")
        print("                /|\ ")
        print("                 | ")
    elif intentos==6:
        print("                 O")
        print("                /|\ ")
        print("                 | ")
        print("                / ")
    elif intentos==7:
        print("                 O")
        print("                /|\ ")
        print("                 | ")
        print("                / \ ")

def imprimir_inicio():
    """
    Muestra un banner indicando que el juego inicio
    @return:
    """
    print(""" 
***************************************************************************
***************************************************************************
**************************    				     **************************
**************************					     **************************
**************************    EL JUEGO INICIO    **************************
**************************   			         **************************
**************************                       **************************
***************************************************************************
*********************************************************************\n""")

def validar_entrada(letra:str):
    """
    Este metodo me valida que el usuario solo ingrese una letra
    @param letra: es una de las letras pertenecientes a la palabra a adivinar
    @return: letra
    """
    letras=["a",'b','c','d','e','f','g','h','i','j','k','l','m','n','ñ','o','p','q','r','s','t','u','v','w','x','y','z']
    while(letra not in letras or len(letra)!=1):
        print('Solo digite una letra')
        letra= input("Digite una letra: ") 
    return letra  

def validar_opcion(opcion):
    """
    Esta funcion se encarga de que el usuario solo pueda digitar un 1  o 2 que son las opciones posibles a elegir
    @param opcion: es solo para hacer una funcion en especifico
    @return:
    """
    opciones=["1","2"]
    while(opcion not in opciones):
        print('Solo digite una opcion valida')
        opcion=input("Digite la opcion: ")
    return opcion

def iniciar_juego():
    """
    Esta funcion se encarga de toda la secuencia del juego con ayuda de las funciones anteriores, se encarga de validar
    que las letras que se ingresen sean solo una validando si o no pertenecen a la palabra adivinar, contabiliza el numero
    de intentos para indicar cuando el usuario pierde, tambien valida si el usuario adivina la palabra por completo para
    indicar que el usuario gano.
    @return:
    """
    palabra=choice(arreglo_palabras)
    palabra_adivinar=["_" for a in palabra]       
    intentos=0
    pregunta=""
    imprimir_inicio()
    print(' '.join(palabra_adivinar))
    while (intentos!=7):
        letra=input("Digite una letra de la palabra: ")
        letra=validar_entrada(letra.lower())

        if(letra in palabra):
            for i in range(len(palabra)):
                if(letra in palabra[i]):
                    palabra_adivinar[i]=letra
            if(palabra==("".join(palabra_adivinar))):                
                imprimir_ganaste()
                print(f'Adivinaste la palabra era {palabra}')
                break
            print(' '.join(palabra_adivinar))
            print("""\n***Quiere adivinar la palabra:***
                    1. Si 2. No""")
            pregunta=input("Digite la opcion: ")
            opcion=validar_opcion(pregunta)
            if(opcion=="1"):
                palabra_aux=input("Digite la palabra que es correctamente: ")
                if(palabra_aux==palabra):
                    imprimir_ganaste()
                    print(f'\nCon solo {intentos} intentos')
                    break
                else:
                    print("Sigue intentandolo\n")
            elif(pregunta=="2"):
               print("")
               continue 
        else:
            intentos+=1
            imprimir_muneco(intentos,palabra)
            print(' '.join(palabra_adivinar))
            print(f'{intentos} intentos\n')
        
        if(intentos==7):
            imprimir_perdiste()
            print(f'La palabra era {palabra}, te acabaste los {intentos} intentos')
            break
        else:
            continue
     
if(__name__=="__main__"):
    iniciar_juego()  