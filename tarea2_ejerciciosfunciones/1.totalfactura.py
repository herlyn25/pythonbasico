def validar_float(numero:str):
    """Es capaz de comprobar si el parametro tiener formato decimal parteentera.parte decimal (0.0)
    @param numero: es el valor a validar que cumpla con el formato de un decimal
    @return: respuesta bool : que indica True si el valor no cumple con formato y False cuando lo cumple
    """

    answer = True
    try:
        if numero.isalpha():
            print("Solo debe contener numeros separados por un punto. Ejemplo 58.59")
        elif numero.find(",") != -1:
            print("El numero solo debe llevar punto no comas. Ejemplo 58.59")
        elif numero.count(".") > 1 or numero.count(",") >= 1 or numero.count(" ") >= 1:
            print("Solo debe contener un punto. Ejemplo 58.59")
        elif numero.count(" ") >= 1:
            print("No debe contener espacios. Ejemplo 58.59")
        elif numero.isnumeric():
            answer = False
        elif float(numero):
            answer = False
        elif numero.isalnum():
            print("No debe contener letras y numeros. Ejemplo 58.59")
    except ValueError:
        print("Digite el formato correcto. Ejemplo 58.59")

    return answer


def calcular_factura(v_factura:float, valor_iva=21.0):
    """
    Calcular el valor de una factura si se le digita un valor de iva lo calcula con ese porcentaje
    sino lo hace con el 21%
    @param v_factura : valor de la factura
    @param valor_iva : valor del iva aplicado a la factura sino se pasa este parametro por defecto asumira 21%
    @return:
    """
    v_a_pagar = v_factura + ((valor_iva / 100) * v_factura)
    print(f' El valor a pagar es {v_a_pagar}')

if __name__ == "__main__":
    valor_factura=input("Digite el valor de la factura")
    while(validar_float(valor_factura)):
        valor_factura = input("Digite el valor de la factura")
    valor_iva = input("Digite el valor del iva")
    if(valor_iva==""):
        calcular_factura(float(valor_factura))
    else:
        while(validar_float(valor_iva)):
            valor_iva = input("Digite el valor del iva")
            if (valor_iva == ""):
                calcular_factura(float(valor_factura))
                break
            else:
                calcular_factura(float(valor_factura),float(valor_iva))
