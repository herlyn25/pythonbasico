def validar_float(numero):
    """
    Es capaz de comprobar si el parametro tiener formato decimal parteentera.parte decimal (0.0)
    @param numero: es el valor a validar que cumpla con el formato de un decimal
    @return: respuesta bool : que indica True si el valor no cumple con formato y False cuando lo cumple
    """

    answer = True
    try:
        if numero.isalpha():
            print("Solo debe contener numeros separados por un punto. Ejemplo 58.59")
        elif numero.find(",") != -1:
            print("El numero solo debe llevar punto no comas. Ejemplo 58.59")
        elif numero.count(".") > 1 or numero.count(",") >= 1 or numero.count(" ") >= 1:
            print("Solo debe contener un punto. Ejemplo 58.59")
        elif numero.count(" ") >= 1:
            print("No debe contener espacios. Ejemplo 58.59")
        elif numero.isnumeric():
            answer = False
        elif float(numero):
            answer = False
        elif numero.isalnum():
            print("No debe contener letras y numeros. Ejemplo 58.59")
    except ValueError:
        print("Digite el formato correcto. Ejemplo 58.59")

    return answer

def ex_2_4(numero_segundos):
    """
    Esta función se encarga de convertir una cantidad X de segundos en dias, horas, minutos y segundos
    @param numero_segundos : Es la cantidad que se toma como base para realizar la conversion
    """
    cont_dias, cont_horas, cont_min = 0, 0, 0
    dias = numero_segundos
    while (dias >= 86400):
        dias -= 86400
        cont_dias += 1
    horas = dias
    while (horas >= 3600):
        horas -= 3600
        cont_horas += 1
    minutos = horas
    while (minutos >= 60):
        minutos -= 60
        cont_min += 1
    segundos = minutos
    print(
        f'{numero_segundos} segundos es igual a {cont_dias} dias, {cont_horas} horas, {cont_min} minutos y {segundos} segundos')


if __name__ == '__main__':
    cantidad_segundos = input("Digite la cantidad de segundos a convertir: ")
    while validar_float(cantidad_segundos):
        cantidad_segundos = input("Digite la cantidad de segundos a convertir: ")
    ex_2_4(float(cantidad_segundos))
