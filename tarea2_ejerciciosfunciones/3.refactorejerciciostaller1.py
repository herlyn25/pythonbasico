def validar_entero(numero):
    """Es capaz de comprobar si el parametro tiener formato entero sin parte decimal (123456789)
    @param numero: es el valor a validar que cumpla con el formato de numero entero
    @return: respuesta bool : que indica True si el valor no cumple con formato y False cuando lo cumple"""

    answer = True
    try:
        if numero == "":
            print("Por favor debe digitar un valor")
        elif numero.isalpha():
            print("Solo debe contener numeros. Ejemplo 144546")
        elif numero.count(".") > 1 or numero.count(",") >= 1 or numero.count(" ") >= 1:
            print("Solo debe contener numeros. Ejemplo 124584")
        elif numero.count(" ") >= 1:
            print("No debe contener espacios. Ejemplo 58.59")
        elif numero.isnumeric():
            answer = False
        elif float(numero):
            print("El numero se aproximara al mas cercano")
            answer = False
        elif numero.isalnum():
            print("No debe contener letras y numeros. Ejemplo 58.59")
    except ValueError:
        print("Digite el formato correcto. Ejemplo 58.59")

    return answer
def validar_float(numero):
    """Es capaz de comprobar si el parametro tiener formato decimal parteentera.parte decimal (0.0)
    @param numero: es el valor a validar que cumpla con el formato de un decimal
    @return: respuesta bool : que indica True si el valor no cumple con formato y False cuando lo cumple
    """

    answer = True
    try:
        if numero.isalpha():
            print("Solo debe contener numeros separados por un punto. Ejemplo 58.59")
        elif numero.find(",") != -1:
            print("El numero solo debe llevar punto no comas. Ejemplo 58.59")
        elif numero.count(".") > 1 or numero.count(",") >= 1 or numero.count(" ") >= 1:
            print("Solo debe contener un punto. Ejemplo 58.59")
        elif numero.count(" ") >= 1:
            print("No debe contener espacios. Ejemplo 58.59")
        elif numero.isnumeric():
            answer = False
        elif float(numero):
            answer = False
        elif numero.isalnum():
            print("No debe contener letras y numeros. Ejemplo 58.59")
    except ValueError:
        print("Digite el formato correcto. Ejemplo 58.59")

    return answer
def validar_numero_par(mi_numero):
    """
    Esta función se encarga de validar solamente si un numero es para o no 
    @param mi_numero : Es el numero qeu se valida para llegar a la conclusión si un numero es par o no.
    """
    if(mi_numero%2==0):
        print(f'{mi_numero} es par')
    else:
        print(f'{mi_numero} es impar')

def calcular_area_triangulo(base,altura): 
    """
    Esta funcion se encarga como su nombre lo dice en calcular el area de un triangulo
    @param base : Es el valor de la base del triangulo
    @param altura : Es el valor de la altura del triangulo
    """
    area_triangulo=(base*altura)/2
    print(f'El area triangulo es: {area_triangulo}')

def sumatoria_numeros_de1an(numero_limite):
    """
    Es una funcion recursiva que se encarga de realizar la sumatoria de los numeros de 1 hasta el parametro
    numerico retornando el valor de esta operación
    @param numero_limite : es el limite hasta donde se realizara la sumatoria
    """
    if(numero_limite>1):
        numero_limite+=sumatoria_numeros_de1an(numero_limite-1)
    return numero_limite

def menu():
    """
    Aqui se ejecutara tota la funcionalidad de la aplicación
    @return:
    """
    banderaInicio = True
    while (banderaInicio):
        print("""
            ****************************************
            **********Menu de Acciones**************
            **** 1 . Validar numero par o impar ****
            **** 2 . Calcular area de triangulo ****
            **** 3 . Sumar numeros de 1 a N ********
            **** 4 . Salir de la aplicación ********
            ****************************************
            """)
        opcion = input("Digite una opcion:")
        if opcion == "1":
            """ Validar si un numero es par o no"""
            un_numero = input("Digite un numero: ")
            while (validar_entero(un_numero)):
                un_numero = input("Digite un numero: ")
            else:
                validar_numero_par(round(float(un_numero)))
        elif (opcion == "2"):
            print("Entro al modulo calcular Area del triangulo")

            base_triangulo = input("Digite la base del tringulo: ")
            while validar_float(base_triangulo):
                base_triangulo = input("Digite la base del tringulo: ")

            altura_triangulo = input("Digite la altura del tringulo: ")
            while validar_float(altura_triangulo):
                altura_triangulo = input("Digite la altura del tringulo: ")
            calcular_area_triangulo(float(base_triangulo), float(altura_triangulo))

        elif (opcion == "3"):
            print("Entro al modulo sumar numeros de 1 a N")
            numero_limite=input("Digite el numero limite: ")
            while(validar_entero(numero_limite)):
                numero_limite = input("Digite el numero limite: ")
            else:
                print(f' La sumatoria de 1 a {numero_limite} es {sumatoria_numeros_de1an(round(float(numero_limite)))}')

        elif (opcion == "4"):
            print("Programa ha finalizado exitosamente.....")
            banderaInicio = False
        else:
            print("Digite una opcion valida")

if __name__ == "__main__":
    menu()